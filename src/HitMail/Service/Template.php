<?php

namespace HitMail\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\ViewModel;
use HitMail\Service\TemplateInterface;

class Template implements TemplateInterface
{
    protected $view;

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->view = $serviceLocator->get('View');
    }

    public function render($mailTemplate, array $data)
    {

        $viewModel = new ViewModel();
        $viewModel->setTemplate($mailTemplate);
        $viewModel->setOption('has_parent', true);
        $viewModel->setVariables($data);

        return $this->view->render($viewModel);
    }

}