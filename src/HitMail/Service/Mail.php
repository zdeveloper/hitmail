<?php

namespace HitMail\Service;

use Zend\ServiceManager\ServiceLocatorInterface;

use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp;
use HitMail\Options\MailOptions;
use HitMail\Service\MailInterface;
use HitMail\Service\Template;

class Mail implements MailInterface
{
    protected $transport;
    protected $options;
    protected $template;
    protected $bcc;

    protected $subject;
    protected $to;
    protected $from;
    protected $data;
    protected $viewTemplate;
    protected $html;
    protected $replyTo;

    public function __construct(Smtp $transport, MailOptions $options, Template $template)
    {
        $this->transport = $transport;
        $this->options = $options;
        $this->template = $template;
    }

    public function execute()
    {
        $html = new MimePart($this->getTemplate()->render($this->getViewTemplate(), $this->getData()));
        $html->type = $this->getOptions()->getType();
        $html->encoding = $this->getOptions()->getHtmlEncoding();

        $body = new MimeMessage();
        $body->setParts(array($html));

        $config = $this->getTransport()->getOptions()->toArray();
        $message = new Message();
        $this->setHtml($body);
        $from = ($this->getFrom() !== null )? $this->getFrom() :$config['connection_config']['from'];
        $message
            ->addFrom($from)
            ->setSubject($this->getSubject())
            ->setBody($this->getHtml())
            ->addHeader('Reply-To',$this->getReplyTo())
            ->setEncoding($this->getOptions()->getMessageEncoding());


        $message->addBcc($this->getBcc());
        if(is_array($this->getTo()[0])) {
            foreach ($this->getTo() as $to => $secondTo) {

                $message->addTo($secondTo[0], $secondTo[1]);

            }
        }else if(is_array($this->getTo())) {

            $message->addTo($this->getTo()[0],$this->getTo()[1]);

        }else if (is_string($this->getTo())) {

            $message->addTo($this->getTo());

        }

        return $message;
    }

    public function send()
    {
        try {
            $this->getTransport()->send($this->execute());

            $evento = new \HitMail\Event\MailEvent();
            $evento->getEventManager()->trigger('emailEnviado', $this, array('content' => $this));

        } catch (\Exception $e) {
            echo $e->getMessage();
        }

    }

    /**
     * Gets the value of transport.
     *
     * @return mixed
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * Sets the value of transport.
     *
     * @param mixed $transport the transport
     *
     * @return self
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;

        return $this;
    }

    /**
     * Gets the value of options.
     *
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Sets the value of options.
     *
     * @param mixed $options the options
     *
     * @return self
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Gets the value of template.
     *
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Sets the value of template.
     *
     * @param mixed $template the template
     *
     * @return self
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Gets the value of subject.
     *
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Sets the value of subject.
     *
     * @param mixed $subject the subject
     *
     * @return self
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Gets the value of to.
     *
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Sets the value of to.
     *
     * @param mixed $to the to
     *
     * @return self
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Gets the value of from.
     *
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Sets the value of from.
     *
     * @param mixed $from the from
     *
     * @return self
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Gets the value of data.
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Sets the value of data.
     *
     * @param mixed $data the data
     *
     * @return self
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Gets the value of viewTemplate.
     *
     * @return mixed
     */
    public function getViewTemplate()
    {
        return $this->viewTemplate;
    }

    /**
     * Sets the value of viewTemplate.
     *
     * @param mixed $viewTemplate the view template
     *
     * @return self
     */
    public function setViewTemplate($viewTemplate)
    {
        $this->viewTemplate = $viewTemplate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * @param mixed $bcc
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * @param mixed $html
     */
    public function setHtml($html)
    {
        $this->html = $html;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * @param mixed $replyTo
     */
    public function setReplyTo($replyTo)
    {
        $this->replyTo = $replyTo;
        return $this;
    }



}